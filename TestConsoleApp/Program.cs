﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestConsoleApp.DAL;

namespace TestConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            using (P2PContext ctx = new P2PContext())
            {
                var people = ctx.People.ToList();
                foreach (var person in people)
                {
                    Console.WriteLine(string.Format("{0}\t{1}\t{2}",
                        person.Id, person.FirstName, person.LastName));
                }
            }


            Console.ReadLine();
        }
    }
}
